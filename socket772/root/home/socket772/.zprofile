if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then

	printf "0)No X11\n1) kdeplasma\n2) cinnamon\n"
	read -r -s -k -t 5 session
	clear

	case $session in
		0) echo "No X11";;
		1) exec startx /usr/bin/startplasma-x11;;
		2) exec startx /usr/bin/cinnamon-session;;
		*) exec startx;;
	esac

fi

export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
export __GL_SHADER_DISK_CACHE_SIZE=100000000000
