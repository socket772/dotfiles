#!/usr/bin/bash
# se un programma ritorna un valore !=0, allora termina automaticamente lo script per evitare catastrofi
set -e

# Passo lo script che avvia il server di minecraft
if [ "$1" = "" ]; then
	echo "non hai detto che server vuoi avviare"
	exit
fi

server_path="$HOME/minecraftserverfolder/$1"
echo "Hai scelto il server $server_path"

if [ "$2" = "DEBUG" ]; then
	bwrap \
--new-session \
--die-with-parent \
--unshare-all \
--share-net \
--uid 1000 \
--gid 1000 \
--hostname server \
--clearenv \
--setenv "OSTYPE" "$OSTYPE" \
--setenv "USER" "$USER" \
--setenv "HOME" "$HOME" \
--setenv "LANG" "$LANG" \
--setenv "PATH" "$PATH" \
--setenv "TERM" "$TERM" \
--setenv "HOST" "$HOST" \
--ro-bind /usr /usr \
--ro-bind /etc/java17-openjdk /etc/java17-openjdk \
--ro-bind /etc/java-8-openjdk /etc/java-8-openjdk \
--ro-bind /etc/java11-openjdk /etc/java11-openjdk \
--ro-bind /etc/java-openjdk /etc/java-openjdk \
--symlink /usr/lib64 /lib64 \
--symlink /usr/lib /lib \
--symlink /usr/bin /bin \
--symlink /usr/bin /sbin \
--symlink /usr/share/zoneinfo/Europe/Rome /etc/localtime \
--ro-bind /etc/passwd /etc/passwd \
--ro-bind /etc/group /etc/group \
--bind-try "$server_path" "$server_path" \
--ro-bind "$HOME/.bashrc" "$HOME/.bashrc" \
--proc /proc \
--tmpfs /run \
--dev /dev \
--tmpfs /tmp \
--tmpfs /var \
bash
exit
fi


cd "$server_path"

bwrap \
--new-session \
--die-with-parent \
--unshare-all \
--share-net \
--uid 1000 \
--gid 1000 \
--hostname server \
--clearenv \
--setenv "OSTYPE" "$OSTYPE" \
--setenv "USER" "$USER" \
--setenv "HOME" "$HOME" \
--setenv "LANG" "$LANG" \
--setenv "PATH" "$PATH" \
--setenv "TERM" "$TERM" \
--setenv "HOST" "$HOST" \
--ro-bind /usr /usr \
--ro-bind /etc/java17-openjdk /etc/java17-openjdk \
--ro-bind /etc/java-8-openjdk /etc/java-8-openjdk \
--ro-bind /etc/java11-openjdk /etc/java11-openjdk \
--ro-bind /etc/java-openjdk /etc/java-openjdk \
--symlink /usr/lib64 /lib64 \
--symlink /usr/lib /lib \
--symlink /usr/bin /bin \
--symlink /usr/bin /sbin \
--symlink /usr/share/zoneinfo/Europe/Rome /etc/localtime \
--ro-bind /etc/passwd /etc/passwd \
--ro-bind /etc/group /etc/group \
--bind-try "$server_path" "$server_path" \
--ro-bind "$HOME/.bashrc" "$HOME/.bashrc" \
--proc /proc \
--tmpfs /run \
--dev /dev \
--tmpfs /tmp \
--tmpfs /var \
date && bash "$server_path/start-server.sh"
