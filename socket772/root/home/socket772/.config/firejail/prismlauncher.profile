# Profile for steam, put it in /etc/firejail or $HOME/.config/firejail
# Tested only on Archlinux, with steam-runtime not native
# IMPORTANT NOTES
# in my case i couldn't add the directive "disable-mnt" since my games where on another disk, if it's not your case, then you can remove the /run/*, /media, /boot, and /efi folders, if you have them.. if it is then add the blaclist for those partitions.
# if you have native games, add to the noblacklist and whitelist those folders if they are somewhere in your home directory.
# Do whatever you want with this profile, i will not be held responsable for any issue caused by this.
# Maybe it will be updated with new things, maybe not, who knows =).


blacklist /mnt/BigData/
blacklist /mnt/SmallData/HeroicGames
blacklist /mnt/SmallData/lost+found
blacklist /mnt/SmallData/SteamLibrary

# Ignore noexec on ${HOME} as PrismLauncher installs LWJGL native
# libraries in ${HOME}/.local/share/PrismLauncher
ignore noexec ${HOME}
ignore noexec /tmp

# Allow java (blacklisted by disable-devel.inc)
include allow-java.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-proc.inc

# Per me
noblacklist /mnt/BigData/_Download
whitelist /mnt/BigData/_Download

# Default data folder
noblacklist ${HOME}/.local/share/PrismLauncher
whitelist ${HOME}/.local/share/PrismLauncher

# Per me
noblacklist /mnt/SmallData/MinecraftFolder/PrismLauncher/
whitelist /mnt/SmallData/MinecraftFolder/PrismLauncher/

#Needed for themeing
noblacklist ${HOME}/.config/kdeglobals
whitelist ${HOME}/.config/kdeglobals

caps.drop all
netfilter
nodvd
nogroups
nonewprivs
private-tmp
noroot
notv
nou2f
novideo
protocol unix,inet,inet6
seccomp
restrict-namespaces
apparmor