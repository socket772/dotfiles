# Profile for steam
# Tested only on Archlinux, with steam-runtime not native

blacklist /boot
blacklist /mnt/BigData
blacklist /mnt/SmallData/MinecraftFolder
blacklist /mnt/SmallData/HeroicGames
blacklist /mnt/SmallData/lost+found
blacklist /sys
blacklist /efi
blacklist /opt
blacklist /srv
blacklist /media
blacklist /run/mount
blacklist /run/media
blacklist ${HOME}/.pki

noblacklist ${HOME}/.local/share/Steam
noblacklist ${HOME}/.local/share/vulkan
noblacklist ${HOME}/.config/StardewValley
noblacklist ${HOME}/.local/share/metro_3
noblacklist ${HOME}/.config/unity3d
noblacklist ${HOME}/.steam
noblacklist ${HOME}/.steampath
noblacklist ${HOME}/.steampid
noblacklist /sbin
noblacklist /usr/sbin

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python2.inc
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc
include whitelist-common.inc
include whitelist-var-common.inc

whitelist ${HOME}/.local/share/Steam
whitelist ${HOME}/.local/share/vulkan
whitelist ${HOME}/.config/StardewValley
whitelist ${HOME}/.local/share/metro_3
whitelist ${HOME}/.config/unity3d
whitelist ${HOME}/.steam
whitelist ${HOME}/.steampath
whitelist ${HOME}/.steampid

caps.drop all
# seccomp # Breaks steam
# apparmor # Breaks steam
netfilter
nodvd
nogroups
nonewprivs
noroot
notv
noprinters
machine-id # can break steam
nou2f
novideo
protocol unix,inet,inet6,netlink
private-dev
private-etc alsa,alternatives,asound.conf,bumblebee,ca-certificates,crypto-policies,dbus-1,drirc,fonts,group,gtk-2.0,gtk-3.0,host.conf,hostname,hosts,ld.so.cache,ld.so.conf,ld.so.conf.d,ld.so.preload,localtime,lsb-release,mime.types,nvidia,os-release,passwd,pki,pulse,resolv.conf,services,ssl,vulkan
private-tmp
