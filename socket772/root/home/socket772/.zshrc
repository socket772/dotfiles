# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Check if the terminal is truecolor
[[ "$COLORTERM" == (24bit|truecolor) || "${terminfo[colors]}" -eq '16777216' ]] || zmodload zsh/nearcolor

# Set terminal to alacritty
export TERM=alacritty

# If not used, sh scripts WILL break, can cause problems with zsh, leave enabled
emulate sh -c 'source /etc/profile'

# Enable scrollwheel for systemctl and journalctl
SYSTEMD_LESS=RSMK

# Enable colors and change prompt:
autoload -U colors && colors

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

####################### XDG Configs #######################
source $HOME/.config/user-dirs.dirs

######################## KEYBINDS ########################
# Set keybinds emacs style
bindkey -e
# Custom Binds
# ^[[A = up arrow
# ^[[B = down arrow
# ^[[C = right arrow
# ^[[D = left arrow
# ^[[5~ = pag up
# ^[[6~ = pag down
#
# IF COMMENTED IT'S ALREADY DEFAULT EMACS BEHAVIOR
#

#keybinds custom
bindkey '^H' backward-delete-word #ctrl+backspace delete previous word
bindkey '^[[1;5D' backward-word #ctrl+left Move to the beginning of the previous word.
bindkey '^[[1;5C' forward-word #ctrl+right Move to the beginning of the next word
bindkey '^[[3;5~' kill-word #ctrl+del delete next word

######################## COMPLETIONS ########################

autoload -Uz compinit && compinit
# zsh-completion menu style
zstyle ':completion:*' menu select
# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Auto cd to path
setopt autocd

# Include hidden files in zsh-completion
_comp_options+=(globdots)

# If you install a new program or an update that adds something to /bin the shell wont recognize this change, with a pacman hook and this part, every time you add/update software, the shell will check if there where any changes, if there are, then it will update itself everytime you launch a command
zshcache_time="$(date +%s%N)"
autoload -Uz add-zsh-hook
rehash_precmd() {
  if [[ -a /var/cache/zsh/pacman ]]; then
    local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
    if (( zshcache_time < paccache_time )); then
      rehash
      zshcache_time="$paccache_time"
    fi
  fi
}
add-zsh-hook -Uz precmd rehash_precmd

######################## ALIASES ########################
alias ls='eza -alho --git --color=always --icons=always --color-scale=all --color-scale-mode=gradient'
alias diff='diff --color'
alias grep='grep --color'
alias help='run-help'

######################## EXTRA ########################
# IDK
typeset -U path PATH
path=(~/.local/bin $path)
export PATH

# To get journalctl colored output (i think)
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
export LESS=' -R '

# Syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Command correction
setopt correct

######################## PROMPT STYLE ########################
# %n = username
# %D = date
# %* = time
# %~ = current path
# %F{colornumber/name} = set color from that point, uses ANSI 8-bit color codes https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
# %f = unset color
# %f = unset color
# %? = return value of last command
PROMPT='%F{118}%n%f %F{red}->%f %F{51}%B%~%b%f %F{118}}%f '
RPROMPT='[%F{yellow}%?%f]'

######################## SECRETS ########################
# Secret stuff (such as ssh key agent and other "shouldn't be public" stuff)
source ~/.config/zsh/secrets.zsh
