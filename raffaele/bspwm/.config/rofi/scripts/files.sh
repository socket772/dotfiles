#!/bin/bash

myTerm=alacritty
myEdit=nvim

menu(){
	printf "1. alacritty\n"
	printf "2. bspwm\n"
	printf "3. sxhkd\n"
	printf "6. polybar\n"
	printf "11. .bashrc\n"
	printf "12. .profile\n"
}

main(){
  choice=$(menu | rofi -dmenu | cut -d. -f1)
	case $choice in
		1)
			$myTerm -e sh -c "sleep 0.2 ; $myEdit $HOME/.config/alacritty/alacritty.yml"
      ;;
		2)
			$myTerm -e sh -c "sleep 0.2 ; $myEdit $HOME/.config/bspwm/bspwmrc"
      ;;
		3)
			$myTerm -e sh -c "sleep 0.2 ; $myEdit $HOME/.config/sxhkd/sxhkdrc"
      ;;
		6)
      $myTerm -e sh -c "sleep 0.2 ; $myEdit $HOME/.config/polybar/config"
      ;;
		11)
      $myTerm -e sh -c "sleep 0.2 ; $myEdit $HOME/.bashrc"
      ;;
		12)
      $myTerm -e sh -c "sleep 0.2 ; $myEdit $HOME/.profile"
      ;;
  esac
}

main

