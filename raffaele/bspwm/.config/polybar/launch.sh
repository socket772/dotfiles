#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Launch bars
echo "---" | tee -a /tmp/polybar1.log
#echo "---" | tee -a /tmp/polybar2.log
#echo "---" | tee -a /tmp/polybar3.log

# main bar
polybar bar1 2>&1 | tee -a /tmp/polybar1.log & disown
#polybar bar2 2>&1 | tee -a /tmp/polybar2.log & disown
#polybar bar3 2>&1 | tee -a /tmp/polybar3.log & disown

echo "Bars launched..."
