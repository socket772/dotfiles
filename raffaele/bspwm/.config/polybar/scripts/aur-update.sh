#!/bin/sh

if ! updates_aur=$(paru -Qum 2> /dev/null | wc -l); then
    updates_aur=0
fi

if [ "$updates_aur" -gt 0 ]; then
    echo " aur $updates_aur"
else
    echo ""
fi
