#!/bin/bash

# 2560x1080
#if [[ $(xrandr -q | grep -e 'DisplayPort-1 connected primary 2560x1080+0+0') ]]; then
  xrandr --output DisplayPort-0 --off \
         --output DisplayPort-1 --primary --mode 2560x1080 --pos 0x0 --rotate normal \
         --output DisplayPort-2 --off \
         --output HDMI-A-0 --off
  bspc monitor DisplayPort-1 -d 1 2 3 4 5 6 7 8 9 10
#fi

# 2560x1080 + 1920x1080
#if [[ $(xrandr -q | grep -e 'DisplayPort-1 connected primary 2560x1080+0+0') && $(xrandr -q | grep -e 'HDMI-A-0 connected 1920x1080+2560+0') ]]; then
#  xrandr --output DisplayPort-0 --off \
#         --output DisplayPort-1 --primary --mode 2560x1080 --pos 0x0 --rotate normal \
#         --output DisplayPort-2 --mode 1920x1080 --pos 2560x0 --rotate normal \
#         --output HDMI-A-0 --off
#  bspc monitor DisplayPort-1 -d 1 2 3 4 5 6 7 8 9
#  bspc monitor DisplayPort-2 -d 10
#fi

# 2560x1080 + 1920x1080 wacom
#if [[ $(xrandr -q | grep -e 'DisplayPort-1 connected primary 2560x1080+0+0') && $(xrandr -q | grep -e 'HDMI-A-0 connected 1920x1080+2560+0') ]]; then
#  xrandr --output DisplayPort-0 --off \
#         --output DisplayPort-1 --primary --mode 2560x1080 --pos 0x0 --rotate normal \
#         --output DisplayPort-2 --off \
#         --output HDMI-A-0 --mode 1920x1080 --pos 2560x0 --rotate normal
#  bspc monitor DisplayPort-1 -d 1 2 3 4 5 6 7 8 9
#  bspc monitor HDMI-A-0 -d 10
#  xsetwacom --set "Wacom One Pen Display 13 Pen stylus" MapToOutput 1920x1080+2560+0
#fi

# 2560x1080 + 1920x1080 + 1920x1080 wacom
#if [[ $(xrandr -q | grep -e 'DisplayPort-1 connected primary 2560x1080+0+0') && $(xrandr -q | grep -e 'DisplayPort-2 connected 1920x1080+2560+0') && $(xrandr -q | grep -e 'HDMI-A-0 connected 1920x1080+2560+1080') ]]; then
#  xrandr --output DisplayPort-0 --off \
#         --output DisplayPort-1 --primary --mode 2560x1080 --pos 0x0 --rotate normal \
#         --output DisplayPort-2 --mode 1920x1080 --pos 2560x0 --rotate normal \
#         --output HDMI-A-0 --mode 1920x1080 --pos 2560x1080 --rotate normal
#  bspc monitor DisplayPort-1 -d 1 2 3 4 5 6 7 8
#  bspc monitor DisplayPort-2 -d 9
#  bspc monitor HDMI-A-0 -d 10
#  xsetwacom --set "Wacom One Pen Display 13 Pen stylus" MapToOutput 1920x1080+2560+1080
#fi
