#!/bin/bash
bspc config normal_border_color "#4c566a"
bspc config focused_border_color "#5e81ac"
bspc config active_border_color "#81a1c1"
bspc config presel_feedback_color "#434c5e"
