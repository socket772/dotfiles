#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
              . /usr/share/bash-completion/bash_completion

# Aliases
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -la'
alias radeontop='radeontop -c'

# CLI
PS1="[\u@\h \W]\$ "

# System
export EDITOR=nvim
export TERM=alacritty

# Git
export GITHUB_USER=PerilousBooklet
export GITLAB_USER=PerilousBooklet

# GTK
export GTK_THEME=Nordic

# QT
export QT_SELECT=5
export QT_STYLE_OVERRIDE=kvantum
export QT_QPA_PLATFORMTHEME=qt5ct

# Mangal
export MANGAL_DOWNLOADER_PATH="~/downloads/manga/"
export MANGAL_READER_PDF="mupdf"

# Arch Linux Development Tools
export CHROOT=$HOME/chroot

